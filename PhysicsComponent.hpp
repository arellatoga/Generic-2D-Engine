#pragma once
#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>
#include <boost/any.hpp>

#include "Component.hpp"

namespace ngn {
    class PhysicsComponent : public Component {
        public:
            virtual void Start(ngn::GameObject *);
            virtual void Update();
            virtual void Initialize();
            float Speed();
        private:
            float speed;
    };
}
