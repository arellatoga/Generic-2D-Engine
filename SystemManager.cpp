#include "SystemManager.hpp"

void ngn::SystemManager::SendMessage(std::string system, ngn::GameObject* go, std::string funcName, std::vector<boost::any> args) {
    for (int i = 0; i < systems.size(); i++) {
        if (systems[i]->systemName == system) {
            systems[i]->ReceiveMessage(go, funcName, args);
        }
    }
}
