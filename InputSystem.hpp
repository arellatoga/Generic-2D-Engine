#pragma once
#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>
#include <boost/any.hpp>

#include "System.hpp"
#include "SystemList.hpp"
#include "ComponentList.hpp"
#include "GameObject.hpp"
#include "Global.hpp"

namespace ngn {
    class InputSystem : public System  {
        public:
            void Start();
            void Update(float);
            void AddComponent(Component*);
            void ReceiveMessage(GameObject*, std::string, std::vector<boost::any>);
        private:
            void Input(InputComponent*);
            std::vector<InputComponent*> components;
    };
}
