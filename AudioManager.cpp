#include "AudioManager.hpp"

ngn::AudioManager::AudioManager() {
}

ngn::AudioManager::~AudioManager() {
}

void ngn::AudioManager::LoadBuffer(std::string name, sf::SoundBuffer buffer) {
    buffers.insert(std::pair<std::string, sf::SoundBuffer>(name, buffer));
}
