// Include all global variables in Global.hpp
// Global
#include "Global.hpp"
#include "GlobalFunctions.hpp"
#include "ComponentList.hpp"
#include "SystemList.hpp"

// C++ Libraries here
#include <iostream>
#include <string>

// SFML Libraries here
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>

int main () {
	window.setFramerateLimit(60);
    inputSystem->Start();
    physicsSystem->Start();
    spriteSystem->Start();

    ngn::FirstScene* firstScene = new ngn::FirstScene();
    firstScene->Initialize();

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}

		window.clear(sf::Color(0, 0, 0, 0));
		//window.clear(sf::Color(255, 255, 255, 0));

        firstScene->Update((float)(1/60));

		window.display();
	}

    firstScene->Terminate();

    // THIS IS NOT FUNNY. DELETE THIS.
    delete firstScene;
    delete inputSystem;
    delete physicsSystem;
    delete textureManager;
    delete audioManager;
}
