/**
 * World class
 * Governs all GameObjects
 * Handles collision if necessary
 */

#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <SFML/Graphics.hpp>
#include "GameObject.hpp"
//#include "Global.hpp"

namespace ngn {
	class World {
	public:
		World(bool);
		~World();
		void AddGameObject(GameObject*);
		void RemoveGameObject(GameObject*);
        void RemoveGameObject(GameObject*, float);
		void MoveGameObject(GameObject*, float, float);
        bool IsIntersecting(GameObject*);
        bool IsIntersectingWith(GameObject*, std::string);
        bool IsIntersectingWith(GameObject*, GameObject*);
        // Subject (World) sends notifications to its observers (GameObjects)
        void SendNotifyCollision(GameObject*, GameObject*);
        void RunGameObjects(float);
	private:
		std::vector<ngn::GameObject*> GameObjects;
	};
}
