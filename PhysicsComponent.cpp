#include "PhysicsComponent.hpp"
#include "Global.hpp"

void ngn::PhysicsComponent::Start(ngn::GameObject* componentOwner) {
    owner = componentOwner;
    id = "PhysicsComponent";
    speed = 500.0 * (1.0/60.0);
}

void ngn::PhysicsComponent::Update() {
    speed = 500.0;
}

void ngn::PhysicsComponent::Initialize() {
}

float ngn::PhysicsComponent::Speed() {
    return speed;
}
