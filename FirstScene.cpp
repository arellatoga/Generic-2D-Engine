#include "FirstScene.hpp"

void ngn::FirstScene::Initialize() {
    ngn::GameObject* player = new ngn::GameObject(0, 0, 100, 100);
    ngn::GameObject* blockade = new ngn::GameObject(200, 0, 10, 10);

    ngn::PhysicsComponent* physics = new ngn::PhysicsComponent();
    ngn::InputComponent* input = new ngn::InputComponent();
    ngn::SpriteComponent* sprite = new ngn::SpriteComponent();
    sprite->AssignTexture("../sprites/squid2.png");

    input->Initialize(0.25);

    AddComponentToGameObject(player, physics);
    AddComponentToGameObject(player, input);
    AddComponentToGameObject(player, sprite);

    inputSystem->AddComponent((ngn::Component*)input); 
    physicsSystem->AddComponent((ngn::Component*)physics);
    spriteSystem->AddComponent((ngn::Component*)sprite);

    AddGameObject(player);
    AddGameObject(blockade);
}

void ngn::FirstScene::Update(float dt) {
    world.RunGameObjects(dt);
    inputSystem->Update(dt);
    physicsSystem->Update(dt);
    spriteSystem->Update(dt);
}

void ngn::FirstScene::Terminate() {
    
}
