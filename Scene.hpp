#pragma once
#include <ctime>

namespace ngn {
    class Scene {
        public:
        virtual void Initialize() = 0;
        virtual void Update(float) = 0;
        virtual void Terminate() = 0;
        float timeElapsed = 0;

    };
}
