#include "World.hpp"

ngn::World::World(bool collisionOn) {
}

ngn::World::~World() {
    std::cout << "Message: Removing " << GameObjects.size() << " amount of GOs" << std::endl;
	for (int i = 0; i < GameObjects.size(); i++) {
		std::cout << "Message: Removing GO" << std::endl;
		delete GameObjects[i];
	}

	std::cout << "Message: GOs removed" << std::endl;
}

void ngn::World::AddGameObject(GameObject* go) {
	GameObjects.push_back(go);
	std::cout << "Message: GO added" << std::endl;
}

void ngn::World::RemoveGameObject(GameObject* go, float lifetime) {
    go->Destroy(lifetime);   
}

void ngn::World::RemoveGameObject(GameObject* go) {
	bool deleted = false;
	for (int i = 0; i < GameObjects.size(); i++) {
		if (GameObjects[i] == go) {
			GameObjects.erase(GameObjects.begin() + i);
			deleted = true;
			delete go;
			break;
		}
	}

	if (!deleted) {
		std::cout << "Warning: Invalid GO!" << std::endl;
	}
	else {
		std::cout << "Message: GO removed" << std::endl;
	}
}

void ngn::World::MoveGameObject(GameObject* go, float x, float y) {
	sf::FloatRect newBox = go->BoundingBox();
	newBox.left += x;
	newBox.top += y;

	for (int i = 0; i < GameObjects.size(); i++) {
		GameObject* other_go = GameObjects[i];
		if (go != other_go && newBox.intersects(other_go->BoundingBox())) {
			// They do intersect
			float deltaX = 0;
			float deltaY = 0;

            // Check for the distance between collider and collidee
			if (x > 0) {
				deltaX = other_go->BoundingBox().left - (go->BoundingBox().left + go->BoundingBox().width);
			}
			else if (x < 0) {
				deltaX = (other_go->BoundingBox().left + other_go->BoundingBox().width) - go->BoundingBox().left;
			}

            if (y > 0) {
                deltaY = other_go->BoundingBox().top - (go->BoundingBox().top + go->BoundingBox().height);
            }
            else if (y < 0) {
                deltaY = (other_go->BoundingBox().top + other_go->BoundingBox().height) - go->BoundingBox().top;
            }

            // if distance between is lesser than the change, move
            if (std::abs(x) > std::abs(deltaX)) {
                newBox.left = newBox.left - x + deltaX;
                x = deltaX;
            }

            if (std::abs(y) > std::abs(deltaY)) {
                newBox.top = newBox.top - y + deltaY;
                y = deltaY;
            }
           
            SendNotifyCollision(go, other_go);
		}
	}

    go->UpdateBoundingBox(newBox);
}

void ngn::World::SendNotifyCollision(GameObject* go1, GameObject* go2) {
    go1->ReceiveNotifyCollision(go2);
    go2->ReceiveNotifyCollision(go1);
}

bool ngn::World::IsIntersecting(GameObject* go) {
    for (int i = 0; i < GameObjects.size(); i++) {
        if (go->BoundingBox().intersects(GameObjects[i]->BoundingBox())) {
            return true;
        }
    }
    return false;
}

bool ngn::World::IsIntersectingWith(GameObject* go, std::string other_go) {
    for (int i = 0; i < GameObjects.size(); i++) {
        if (GameObjects[i]->UID() == other_go) {
            if (go->BoundingBox().intersects(GameObjects[i]->BoundingBox())) {
                return true;
            }
        }
    }
    return false;
}

bool ngn::World::IsIntersectingWith(GameObject* go, GameObject* other_go) {
    return IsIntersectingWith(go, other_go->UID());
}

void ngn::World::RunGameObjects(float dt) {
    for (int i = 0; i < GameObjects.size(); i++) {
        GameObjects[i]->Update(dt);
    }
}
