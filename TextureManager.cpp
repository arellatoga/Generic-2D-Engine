#include "TextureManager.hpp"

ngn::TextureManager::TextureManager() {
    // do nothing
}

ngn::TextureManager::~TextureManager() {  
}

void ngn::TextureManager::LoadTexture(std::string name) {
    sf::Texture texture;
    if (!texture.loadFromFile(name)) {
        std::cout << "Failed to load texture!" << std::endl;
    }
    else {
        std::cout << "Successfully loaded " << name << std::endl;
    }
    textures.insert(std::pair<std::string, sf::Texture>(name, texture));
}

void ngn::TextureManager::LoadTexture(std::string name, sf::Texture texture) {
    textures.insert(std::pair<std::string, sf::Texture>(name, texture));
}

sf::Texture& ngn::TextureManager::GetTexture(std::string texName) {
    return textures.find(texName)->second;
}
