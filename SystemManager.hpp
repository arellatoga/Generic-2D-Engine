#pragma once

#include "Global.hpp"
#include "SystemList.hpp"
#include "GameObject.hpp"
#include "System.hpp"
#include <boost/any.hpp>

#include <vector> 

namespace ngn {
    class SystemManager{
    public:
        void SendMessage(std::string, ngn::GameObject*, std::string, std::vector<boost::any>);
    private:
        std::vector<System*> systems;
    };
};
