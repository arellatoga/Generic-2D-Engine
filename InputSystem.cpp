#include "InputSystem.hpp"

void ngn::InputSystem::Start() {
    systemName = "InputSystem";
    componentName = "InputComponent";
}

void ngn::InputSystem::Update(float dt) {
    for (int i = 0; i < components.size(); i++) {
        Input(components[i]);
    }
}

void ngn::InputSystem::AddComponent(Component* comp) {
    InputComponent* input = dynamic_cast<InputComponent*>(comp);
    if (comp->ID() == componentName) {
        std::cout << "Added" << std::endl;
        components.push_back(input);
    }
    else {
        std::cout << "Warning: Invalid Component!" << std::endl;
    }
}

void ngn::InputSystem::Input(InputComponent* comp) {
    float move_x = 0;
    float move_y = 0;
    std::vector<boost::any> axes;

    if (comp->GetCooldownCurrent() > 0){
        comp->SetCooldownTimer(comp->GetCooldownCurrent() - (1.0/60.0));
    }
    else {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
            comp->SetCooldownTimer(comp->GetCooldown());
            std::cout << "Input!" << std::endl;
            ngn::GameObject* proj = new ngn::GameObject(250, 250, 50, 50);
            ngn::SpriteComponent* projSprite = new ngn::SpriteComponent();
            projSprite->AssignTexture("../sprites/squid2.png");
            AddComponentToGameObject(proj, projSprite);
            spriteSystem->AddComponent((ngn::Component*)projSprite);
            AddGameObject(proj);
            proj->Destroy(0.33);
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        move_x = -1;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        move_x = 1;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        move_y = -1;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
        move_y = 1;
    }

    axes.push_back(move_x);
    axes.push_back(move_y);
   
    PhysicsSystem* _physicsSystem;
    _physicsSystem = (PhysicsSystem*) physicsSystem;
    _physicsSystem->Move(comp->owner, move_x, move_y);
}

void ngn::InputSystem::ReceiveMessage(ngn::GameObject* go, std::string funcName, std::vector<boost::any> args) {
    // do nothing for now;
    // actually, DELETE THIS
}
