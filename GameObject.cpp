#include "GameObject.hpp"

ngn::GameObject::GameObject(int _x, int _y, int _w, int _h) {
	bBox = sf::FloatRect(_x, _y, _w, _h);
    destroySelf = false;
}
ngn::GameObject::GameObject(sf::Vector2f _pCoords, sf::Vector2f _bBox) {
	bBox = sf::FloatRect(_pCoords, _bBox);
    destroySelf = false;
}

void ngn::GameObject::Update(float fl) {
    std::cout << "update?" << std::endl;
    
    if (destroySelf) {
        lifetime -= 1.0/60.0;
        if (lifetime <= 0) {
            world.RemoveGameObject(this);
        }
    }
}

float ngn::GameObject::Lifetime(){
    return lifetime;
}

void ngn::GameObject::UpdateBoundingBox(sf::FloatRect _bBox) {
	bBox = _bBox;
}

void ngn::GameObject::AddComponent(ngn::Component* comp) {
    components.push_back(comp);
}

sf::Vector2f ngn::GameObject::PointCoords() {
	return sf::Vector2f(bBox.left, bBox.top);
}

sf::Vector2f ngn::GameObject::Dimensions(){
	return sf::Vector2f(bBox.width, bBox.height);
}

sf::FloatRect ngn::GameObject::BoundingBox() {
	return bBox;
}

void ngn::GameObject::SetID(std::string _id) {
	id = _id;
}

void ngn::GameObject::SetTag(std::string _tag) {
	tag = _tag;
}

void ngn::GameObject::SetLayer(std::string _layer) {
	layer = _layer;
}

std::string ngn::GameObject::UID() {
    return uid;
}

void ngn::GameObject::ReceiveNotifyCollision(GameObject* other_go) {
    //std::cout << "Message: Collision!" << std::endl;
}

void ngn::GameObject::Destroy(float _time) {
    lifetime = _time;
    destroySelf = true;
}
