#include "SpriteSystem.hpp"

void ngn::SpriteSystem::Start() {
    systemName = "SpriteSystem";
    componentName = "SpriteComponent";
}

void ngn::SpriteSystem::Update(float dt) {
    //std::cout << components.size() << std::endl;
    for (int i = 0; i < components.size(); i++) {
        components[i]->Update();
    }

    for (int i = 0; i < components.size(); i++) {
        components[i]->Draw();
    }
}

void ngn::SpriteSystem::AddComponent(ngn::Component* comp) {
    SpriteComponent* sprite = dynamic_cast<SpriteComponent*>(comp);
    if (comp->ID() == componentName) {
        components.push_back(sprite);
    }
    else {
        std::cout << "Warning: Invalid Component!" << std::endl;
    }
}

void ngn::SpriteSystem::ReceiveMessage(ngn::GameObject* go, std::string funcName, std::vector<boost::any> args) {

}
