#include "Global.hpp"

sf::RenderWindow window(sf::VideoMode(800, 600), ">enginedevving", sf::Style::Titlebar);
ngn::World world(true);
ngn::TextureManager* textureManager = new ngn::TextureManager();
ngn::AudioManager* audioManager = new ngn::AudioManager();

ngn::System* inputSystem = new ngn::InputSystem();
ngn::System* physicsSystem = new ngn::PhysicsSystem();
ngn::System* spriteSystem = new ngn::SpriteSystem();

