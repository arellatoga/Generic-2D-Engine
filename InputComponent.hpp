#pragma once
#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>
#include <boost/any.hpp>

#include "Component.hpp"

namespace ngn {
    class InputComponent : public Component {
    public:
        virtual void Start(ngn::GameObject*);
        virtual void Update();
        virtual void Initialize();
        virtual void Initialize(float);

        float GetCooldown();
        float GetCooldownCurrent();
        void SetCooldownTimer(float);

    private:
        void Movement();
        float speed;
        float projectileCooldown;
        float projectileCooldownCurrent;
    };
}
