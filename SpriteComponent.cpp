#include "SpriteComponent.hpp"
#include "GlobalFunctions.hpp"

void ngn::SpriteComponent::Start(ngn::GameObject* go) {
    owner = go;
    id = "SpriteComponent";
}

void ngn::SpriteComponent::Update() {
    pCoords = owner->PointCoords();
    sprite.setPosition(pCoords);
}

void ngn::SpriteComponent::Initialize() {
}

void ngn::SpriteComponent::Draw() {
    window.draw(sprite);
}

void ngn::SpriteComponent::AssignTexture(std::string texName) {
    LoadTexture(texName);
    tex = GetTexture(texName);
    sprite.setTexture(tex);
    sprite.setScale(4, 4);
}
