#pragma once
#include "Scene.hpp"
#include "Global.hpp"
#include "GlobalFunctions.hpp"

namespace ngn {
    class FirstScene : public Scene {
    public:
        void Initialize();
        void Update(float);
        void Terminate();
    private:
    };
} 
