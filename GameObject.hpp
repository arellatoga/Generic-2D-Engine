#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

#include <SFML/Graphics.hpp>
#include <boost/any.hpp>

#include "ComponentList.hpp"
#include "GameObject.hpp"
//#include "World.hpp"
//#include "Component.hpp"

namespace ngn {
	class GameObject {
	public:
		GameObject(int, int, int, int);
		GameObject(sf::Vector2f, sf::Vector2f);
        // primary Update function CALL THIS EVERY FRAME
		void Update(float);
        // Set ID 
		void SetID(std::string);
		void SetTag(std::string);
        // Set Layer
		void SetLayer(std::string);
        // Update bounding Box
		void UpdateBoundingBox(sf::FloatRect);
        // return Dimensions
        void AddComponent (ngn::Component*);
		sf::Vector2f Dimensions();
        // return Point Coordinates
		sf::Vector2f PointCoords();
        // return Dimensions and Point Coordinates in one convenient variable
		sf::FloatRect BoundingBox();
        std::string UID();
        void Destroy(float);
        float Lifetime();

        ////
        // Subject receive notification functions here
        // Received notifications regarding collisions
        void ReceiveNotifyCollision(GameObject*);

	private:
        bool destroySelf;
        float lifetime;
		std::vector<ngn::Component*> components;
		sf::FloatRect bBox;
		std::string id; // individual ID
		std::string tag; // tag
		std::string layer; // layer
		std::string uid; // randomly generated id;
	};
}
