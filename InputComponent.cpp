#include <iostream>

#include "InputComponent.hpp"
#include "Global.hpp"

void ngn::InputComponent::Start(ngn::GameObject* componentOwner) {
    owner = componentOwner;
    id = "InputComponent";
}

void ngn::InputComponent::Update() {
    Movement();
}

void ngn::InputComponent::Initialize() {
    speed = 20.0;
    projectileCooldown = 0.24;
    projectileCooldownCurrent = 0;
}

float ngn::InputComponent::GetCooldown(){
    return projectileCooldown;   
}

float ngn::InputComponent::GetCooldownCurrent(){
    return projectileCooldownCurrent;
}

void ngn::InputComponent::SetCooldownTimer(float _time){
    projectileCooldownCurrent = _time;
}

void ngn::InputComponent::Initialize(float _projectileCooldown) {
    projectileCooldown = _projectileCooldown;
    projectileCooldownCurrent = 0.0;
}

void ngn::InputComponent::Movement() {
    float move_x = 0;
    float move_y = 0;
    std::vector<boost::any> axes;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        move_x = -1;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        move_x = 1;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        move_y = -1;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
        move_y = 1;
    }

    axes.push_back(move_x);
    axes.push_back(move_y);

    //owner->SendMessageToComponent("PhysicsComponent", "Move", axes);
    //physicsSystem->ReceiveMessage(owner, "Move", axes);
    //(PhysicsSystem*)physicsSystem->Move(owner, move_x, move_y);
    PhysicsSystem* physics = (PhysicsSystem*) physicsSystem;
    physics->Move(owner, move_x, move_y);
}
