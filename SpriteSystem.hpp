#pragma once
#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>
#include <boost/any.hpp>

#include "System.hpp"
#include "SystemList.hpp"
#include "ComponentList.hpp"
#include "GameObject.hpp"
#include "Global.hpp"

namespace ngn {
    class SpriteSystem : public System {
    public:
        void Start();
        void Update(float);
        void AddComponent(Component*);
        void ReceiveMessage(GameObject*, std::string, std::vector<boost::any>);
    private:
        std::vector<SpriteComponent*> components;
    };
}
