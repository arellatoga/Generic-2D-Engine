/* 
 * Modify and initialize all the global variables here.
 * Most especially the world class.
 * It's not a singleton but let's pretend that it's one!
 */

#pragma once
#include <SFML/Graphics.hpp>
#include "World.hpp"
#include "TextureManager.hpp"
#include "AudioManager.hpp"
#include "ComponentList.hpp"
#include "SystemList.hpp"
#include "Scene.hpp"
#include "FirstScene.hpp"

extern sf::RenderWindow window;
extern ngn::World world;
extern ngn::TextureManager* textureManager;
extern ngn::AudioManager* audioManager;

extern void AddComponentToGameObject(ngn::GameObject*, ngn::Component*);
extern void AddGameObject(ngn::GameObject*);
extern sf::Texture& GetTexture(std::string);
extern void LoadTexture(std::string, sf::Texture);
extern void LoadTexture(std::string);

extern ngn::System* inputSystem;
extern ngn::System* physicsSystem;
extern ngn::System* spriteSystem;
