#include <iostream>
#include <string>
#include <map>

#include <SFML/Graphics.hpp>
#include <boost/any.hpp>

namespace ngn {
    class TextureManager {
        public:
            TextureManager();
            ~TextureManager();

            void LoadTexture(std::string);
            void LoadTexture(std::string, sf::Texture);
            sf::Texture& GetTexture(std::string);
        private:
            std::map<std::string, sf::Texture> textures;
    };
}
