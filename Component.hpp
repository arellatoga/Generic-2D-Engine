#pragma once
#include <iostream>
#include <string>
#include <boost/any.hpp>

#include <SFML/Graphics.hpp>

namespace ngn {
	// forward declaration
	class GameObject;

	class Component {
	public:
		//virtual Component() = 0;
		virtual void Start(GameObject*) = 0;
		virtual void Update() = 0;
        virtual void Initialize() = 0;
        std::string ID();
		GameObject* owner;
		//virtual ~Component() {}
	protected:
		std::string id;
		std::string uid;
	};
}
