#pragma once
#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>

#include "Component.hpp"

namespace ngn {
	class TestComponent : public Component {
	public:
		virtual void Start(ngn::GameObject*);
		virtual void Update();
		//~TestComponent() {}
	private:
		std::string ThingyWingy;
	};
}
