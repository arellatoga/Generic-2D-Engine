#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <boost/any.hpp>

namespace ngn {
    class Component;
    class GameObject;

    class System  {
    public:
        virtual void Start() = 0;
        virtual void Update(float) = 0;
        virtual void AddComponent(Component*) = 0;
        virtual void ReceiveMessage(GameObject*, std::string, std::vector<boost::any>) = 0;
        std::string systemName;
        std::string componentName;

    protected:
    };
}
