#include <SFML/Audio.hpp>
#include <map>

#include <string>

namespace ngn {
    class AudioManager {
    public:
        AudioManager();
        ~AudioManager();

        void LoadBuffer(std::string, sf::SoundBuffer);
        sf::SoundBuffer& GetBuffer(std::string);
    private:
        std::map<std::string, sf::SoundBuffer> buffers;
    };
}
