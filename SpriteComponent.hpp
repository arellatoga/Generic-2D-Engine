#pragma once
#include <SFML/Graphics.hpp>
#include <string>

#include <boost/any.hpp>
#include "Component.hpp"

namespace ngn {
    class SpriteComponent : public Component {
    public:
        virtual void Start(ngn::GameObject*);
        virtual void Update();
        virtual void Initialize();
        void AssignTexture(std::string);
        void Draw();
    private:
        sf::Texture tex;
        sf::Sprite sprite;
        sf::Vector2f pCoords;
    };
}
