/* Header file containing all the Components.
 * Put them all here!
 * Give them a description, too.
 */

// Base Component class
#include "Component.hpp"
// Test component, outputs "thingywingy" every update frame when added
#include "TestComponent.hpp"
// Enables input
#include "InputComponent.hpp"
// Enables Physics interactions
#include "PhysicsComponent.hpp"
// Sprites
#include "SpriteComponent.hpp"
