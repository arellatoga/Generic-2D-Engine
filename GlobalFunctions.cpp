#include "GlobalFunctions.hpp"

void AddComponentToGameObject(ngn::GameObject* go, ngn::Component* comp) {
    go->AddComponent(comp);
    comp->Start(go);
}

void AddGameObject(ngn::GameObject* go) {
    world.AddGameObject(go);
}

sf::Texture& GetTexture(std::string texture) {
    return textureManager->GetTexture(texture);
}

void LoadTexture(std::string texName, sf::Texture texture) {
    textureManager->LoadTexture(texName, texture);
}

void LoadTexture(std::string texName) {
    textureManager->LoadTexture(texName);
}
