#include "PhysicsSystem.hpp"

void ngn::PhysicsSystem::Start() {
    systemName = "PhysicsSystem";
    componentName = "PhysicsComponent";
}

void ngn::PhysicsSystem::Update(float dt){
    for (int i = 0; i < components.size(); i++) {
        //std::cout << components[i]->ID() << std::endl;
    }
}

void ngn::PhysicsSystem::AddComponent(Component* comp) {
    PhysicsComponent* physics = dynamic_cast<PhysicsComponent*>(comp);
    if (comp->ID() == componentName) {
        components.push_back(physics);
    }
    else {
        std::cout << "Warning: Invalid Component!" << std::endl;
    }
}

void ngn::PhysicsSystem::Move(GameObject* go, float x, float y) {
    for (int i = 0; i < components.size(); i++) {
        if (components[i]->owner == go) {
            world.MoveGameObject(go, x * components[i]->Speed(), y * components[i]->Speed());
            break;
        }
    }
}

void ngn::PhysicsSystem::ReceiveMessage(ngn::GameObject* go, std::string funcName, std::vector<boost::any> args) {
    // do nothing
}
